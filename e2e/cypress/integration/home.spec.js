/* eslint-disable no-unused-expressions */
import Rekaizen from './pages/Rekaizen'

describe('Landing Page has a button', function () {
  it('with a description', function () {
    const rekaizen = new Rekaizen().buttonStartRetro()
    expect(rekaizen.contains('Empieza!')).to.be.true
  })
  it('that when clicked it dissapears', function () {
    const rekaizen = new Rekaizen().startRetro()
    expect(rekaizen.dontExist('#button-start-retro')).to.be.true
  })
  it('that when clicked it calls retro page', function () {
    const rekaizen = new Rekaizen().startRetro()
    expect(rekaizen.exist('#retro-page')).to.be.true
  })
})
