/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
import Rekaizen from './pages/Rekaizen'

describe('Ensemble view', function () {
  it('has a title', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .confirmIssue()
      .exist('#impressions-title')
  })
  it('has a text area', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .confirmIssue()
      .impressionsTextareaToggle()
      .exist('#impressions-textarea')
  })
  it('has a button', function () {
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue()
      .clickOnPropose()
      .confirmIssue()
      .exist('#impressions-button')
  })

  it('adds a new impresion by pressing the "Enter" key', () => {
    const SAMPLE_IMPRESSION = 'Hola mundo impression'
    const rekaizen = new Rekaizen()
      .startRetro()
      .typeIssue('new issue')
      .clickOnPropose()
      .confirmIssue()
      .impressionsTextareaToggle()
      .typeEnterTextareaImpression(SAMPLE_IMPRESSION)
      .listImpressions()
    expect(true).to.be.true
  })
})
