import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import ImprovementActionAdd from '../../src/views/ensembles/ImprovementActionEnsemble'

describe('ImproventActionAdd', () => {
  const id = {
    title: 'add-improvement-action',
    buttonToggle: 'button-show-add-improvement-action',
    textarea: 'textarea-improvement',
    button: 'improvement-action-confirm',
    list: 'list-improvement'
  }
  test('ensemble doesnt render when prop "show" is unset', () => {
    const { container } = render(<ImprovementActionAdd/>)
    const componentContent = container.firstChild

    expect(componentContent).toBe(null)
  })
  test('ensemble renders when prop "show" is set', () => {
    const { container } = render(<ImprovementActionAdd show={true} id={id}/>)
    const componentContent = container.firstChild

    expect(componentContent).not.toBe(null)
  })
  test('renders a confirm button', () => {
    const { container } = render(<ImprovementActionAdd show={true} id={id} showAddItems={true}/>)
    const confirmButton = container.querySelector('#improvement-action-confirm')
    expect(confirmButton).not.toBe(null)
  })

  test('renders a textarea', () => {
    const { container } = render(<ImprovementActionAdd show={true} id={id} showAddItems={true}/>)

    const textArea = container.querySelector('textarea')
    expect(textArea).not.toBe(null)
  })
  test('dispatches onImprovementActionSubmit event', () => {
    const handleSubmit = jest.fn()
    const { container } = render(<ImprovementActionAdd itemsubmit={handleSubmit} show={true} id={id} showAddItems={true}/>)
    const confirmButton = container.querySelector('#improvement-action-confirm')

    fireEvent.click(confirmButton)

    expect(handleSubmit).toHaveBeenCalledTimes(1)
  })
  test('dispatches onChange event on textarea change', () => {
    const onChange = jest.fn()
    const { container } = render(<ImprovementActionAdd onChange={onChange} show={true} id={id} showAddItems={true}/>)
    const textarea = container.querySelector('textarea')

    const SAMPLE_TEXT = 'lorem ipsum'

    fireEvent.change(textarea, {
      target: { value: SAMPLE_TEXT }
    })

    expect(onChange).toHaveBeenCalledTimes(1)
  })
})
