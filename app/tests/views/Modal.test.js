import React from 'react'
import { render, fireEvent, cleanup } from 'react-testing-library'

import Modal from '../../src/views/Modal'

describe('Modal Component', () => {
  afterEach(cleanup)

  test('clicking close button fires onModalClose', () => {
    const handleClose = jest.fn()

    const { getByTestId } = render(
      <Modal onModalClose={handleClose} />
    )

    const closeButton = getByTestId('button-close')
    fireEvent.click(closeButton)
    expect(handleClose).toHaveBeenCalledTimes(1)
  })

  test('clicking background fires onModalClose', () => {
    const handleClose = jest.fn()

    const { getByTestId } = render(
      <Modal onModalClose={handleClose} />
    )

    const background = getByTestId('background-close')
    fireEvent.click(background)
    expect(handleClose).toHaveBeenCalledTimes(1)
  })

  test('should display children', () => {
    const title = 'hello from my modal'
    const { getByText } = render(
      <Modal>
        <h1>{title}</h1>
      </Modal>
    )
    expect(getByText(title)).toBeTruthy()
  })
})
