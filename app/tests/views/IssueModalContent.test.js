/* eslint-disable no-unused-expressions */
import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import IssueModalContent from '../../src/views/IssueModalContent'

describe('Issue has a cancel button which', () => {
  test('when clicked fires the cancel event', () => {
    const props = {
      showConfirmModal: true,
      issueModalText: {
        description: ''
      },
      cancelIssue: jest.fn()
    }
    const { getByTestId } = render(<IssueModalContent {...props} />)
    const cancelButton = getByTestId('button-cancel')
    fireEvent.click(cancelButton)
    expect(props.cancelIssue).toHaveBeenCalledTimes(1)
  })
})

describe('IssueModalContent', () => {
  test('renders the title, the description, the buttons and THE ISSUE', () => {
    const IssueModalText = {
      title: 'title',
      statement: 'description',
      cancel: 'cancel',
      confirm: 'confirm'
    }
    const issue = 'THE ISSUE'

    const { getByText } = render(<IssueModalContent showConfirmModal={true} issueModalText={IssueModalText} issue={issue} />)

    const titleRendered = getByText(IssueModalText.title)
    const statementRendered = getByText(IssueModalText.statement)
    const cancelButtonRendered = getByText(IssueModalText.cancel)
    const confirmButtonRendered = getByText(IssueModalText.confirm)
    const issueRendered = getByText(issue)

    expect(titleRendered).not.toBe.undefined
    expect(statementRendered).not.toBe.undefined
    expect(cancelButtonRendered).not.toBe.undefined
    expect(confirmButtonRendered).not.toBe.undefined
    expect(issueRendered).not.toBe.undefined
  })
})
