import React from 'react'
import { render } from 'react-testing-library'
import ImprovementActionList from '../../src/views/ensembles/ImprovementActionEnsemble'

describe('ImprovementActionList', () => {
  const Props = {
    title: 'Acciones de mejora',
    improvementActions: [1, 2, 3],
    show: true,
    id: {
      title: 'add-improvement-action',
      buttonToggle: 'button-show-add-improvement-action',
      textarea: 'textarea-improvement',
      button: 'improvement-action-confirm',
      list: 'list-improvement'
    }
  }
  test('the title of the card', () => {
    const { getByText } = render(
      <ImprovementActionList
        title={Props.title}
        items={Props.improvementActions}
        show={Props.show}
        id={Props.id}
      />
    )

    const title = getByText(Props.title)

    expect(title).not.toBe(undefined)
  })

  test('renders the list of improve actions', () => {
    const { container } = render(
      <ImprovementActionList
        items = {Props.improvementActions}
        show={Props.show}
        id={Props.id}
      />
    )

    const actionsList = container.querySelector('#list-improvement')
    expect(actionsList.childElementCount).toBe(3)
  })
})
