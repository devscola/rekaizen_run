import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import Button from '../../src/views/Button'

const PRIMARY_CLASS = 'is-dark'

describe('Button', () => {
  test('renders a children', () => {
    const sampleText = 'Lorem ipsum'
    const { getByText } = render(<Button>{sampleText}</Button>)

    const children = getByText(sampleText).textContent
    expect(children).toBe(sampleText)
  })

  test('onClick event works', () => {
    const handleClick = jest.fn()
    const { container } = render(<Button onClick={handleClick} />)
    const button = container.querySelector('button')
    fireEvent.click(button)
    expect(handleClick).toHaveBeenCalledTimes(1)
  })

  test('has class button', () => {
    const { container } = render(<Button />)
    const button = container.querySelector('button')
    expect(button.classList.contains('button')).toBeTruthy()
  })

  test('primary has class is-dark', () => {
    const { container } = render(<Button primary />)
    const button = container.querySelector('button')
    expect(button.classList.contains(PRIMARY_CLASS)).toBeTruthy()
  })

  test('confirm has class is-dark', () => {
    const { container } = render(<Button confirm />)
    const button = container.querySelector('button')
    expect(button.classList.contains('is-dark')).toBeTruthy()
  })

  test('cancel has class is-light', () => {
    const { container } = render(<Button cancel />)
    const button = container.querySelector('button')
    expect(button.classList.contains('is-light')).toBeTruthy()
  })

  test('can receive a custom class', () => {
    const customClass = 'my-class'
    const { container } = render(<Button primary className={customClass} />)
    const button = container.querySelector('button')
    expect(button.classList.contains(customClass)).toBeTruthy()
    expect(button.classList.contains(PRIMARY_CLASS)).toBeTruthy()
  })

  test('loading has class is-loading', () => {
    const { container } = render(<Button primary loading />)
    const button = container.querySelector('button')
    expect(button.classList.contains('is-loading')).toBeTruthy()
    expect(button.classList.contains(PRIMARY_CLASS)).toBeTruthy()
  })
})
