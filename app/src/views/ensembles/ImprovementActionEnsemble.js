import React, { Component } from 'react'
import TextAreaAutoSize from '../TextAreaAutoSize'
import Button from '../Button'
import ListWithTrash from '../ListWithTrash'
import SmartGuide from '../SmartGuide'

class ImprovementActionEnsemble extends Component {
  renderSmartGuide () {
    const {
      handleCloseSmartGuide,
      showSmartGuide
    } = this.props
    if (showSmartGuide) {
      return (
        <SmartGuide
          handleCloseSmartGuide={handleCloseSmartGuide}
        />

      )
    } return null
  }

  renderNewImprovementAction () {
    const {
      id,
      showAddItems,
      placeholder,
      itemsubmit,
      closeWithEsc,
      onChange,
      value,
      confirmAction,
      handleShowSmartGuide
    } = this.props

    if (showAddItems) {
      return (
        <div className="field">
          <div className="control">
            <TextAreaAutoSize
              id={id.textarea}
              placeholder={placeholder}
              itemsubmit={itemsubmit}
              onKeyDown={closeWithEsc}
              onChange={event => onChange(event.target.value)}
              value={value}
            />
          </div>
          <div className="field is-grouped is-grouped-right">
            <div className="buttons">
              <div className="control">
                <Button
                  info
                  id={id.buttonSmart}
                  className='is-pulled-right is-margin-small'
                  onClick={handleShowSmartGuide}
                >
                  <span className="icon has-text-grey">
                    <i className="fas fa-lg fa-info-circle"/>
                  </span>
                </Button>
              </div>
              <div className="control">
                <Button
                  confirm
                  id={id.button}
                  className='is-pulled-right is-margin-small'
                  onClick={itemsubmit}
                >
                  {confirmAction}
                </Button>
              </div>
            </div>
          </div>
        </div>)
    }
    return null
  }

  render () {
    const {
      id,
      show,
      title,
      handleToggle,
      buttonTextToggle,
      items,
      handleClick,
      showSmartGuide
    } = this.props

    if (show) {
      return (
        <div className="section">
          <div className="card is-rounded">
            <header className="card-header">
              <p className="card-header-title"
                data-testid={id.title}
                id={id.title}>
                {title}
              </p>
              <a
                id={id.buttonToggle}
                className="card-header-icon"
                onClick={handleToggle}>
                <strong>{buttonTextToggle}</strong>
              </a>
            </header>
            <div className="card-content">
              <div className="content">
                { this.renderNewImprovementAction() }
                {showSmartGuide ? <div id="smart-guide">Smart</div> : null}
                <ListWithTrash id={id.list} items={items} handleClick={handleClick} />
              </div>
            </div>
          </div>
          <div>
            { this.renderSmartGuide() }
          </div>
        </div>
      )
    }
    return null
  }
}

ImprovementActionEnsemble.defaultProps = {
  handleCloseSmartGuide: () => {},
  handleClick: () => {},
  confirmAction: '',
  itemsubmit: () => {},
  onChange: () => {},
  closeWithEsc: () => {},
  handleToggle: () => {},
  handleShowSmartGuide: () => {},
  buttonTextToggle: '',
  showAddItems: false,
  showSmartGuide: false,
  id: {
    textArea: '',
    title: '',
    buttonToggle: '',
    list: '',
    button: '',
    buttonSmart: ''
  },
  placeholder: '',
  value: '',
  show: false,
  title: '',
  items: []
}

export default ImprovementActionEnsemble
