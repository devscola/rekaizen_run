import React, { Component } from 'react'
import TextAreaAutoSize from '../TextAreaAutoSize'
import List from '../List'

class Impressions extends Component {
  renderNewImpresion () {
    const {
      showAddItems,
      id,
      placeholder,
      itemsubmit,
      closeWithEsc,
      onChange,
      value } = this.props

    if (showAddItems) {
      return (
        <div className="field">
          <div className="control">
            <TextAreaAutoSize
              id={id.textArea}
              placeholder={placeholder}
              itemsubmit={itemsubmit}
              onKeyDown={closeWithEsc}
              onChange={event => onChange(event.target.value)}
              value={value}
            />
          </div>
        </div>
      )
    }
    return null
  }
  render () {
    const {
      id,
      show,
      title,
      handleToggle,
      buttonTextToggle,
      items
    } = this.props

    if (show) {
      return (
        <div className="section">
          <div className="card is-rounded">
            <header className="card-header">
              <p
                className="card-header-title"
                data-testid={id.title}
                id={id.title}
              >
                {title}
              </p>
              <a
                id={id.buttonToggle}
                className="card-header-icon"
                onClick={handleToggle}
              >
                <strong>{buttonTextToggle}</strong>
              </a>
            </header>
            <div className="card-content">
              <div className="content">
                { this.renderNewImpresion() }
                <List id={id.list} items={items} />
              </div>
            </div>
          </div>
        </div>
      )
    }
    return null
  }
}

Impressions.defaultProps = {
  itemsubmit: () => {},
  onChange: () => {},
  closeWithEsc: () => {},
  handleToggle: () => {},
  buttonTextToggle: '',
  showAddItems: false,
  id: {
    textArea: '',
    title: '',
    buttonToggle: '',
    list: ''
  },
  placeholder: '',
  value: '',
  show: false,
  title: '',
  items: []
}

export default Impressions
