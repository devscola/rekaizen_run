import React from 'react'

const Modal = props => {
  return (
    <div className="modal is-active">
      <div className="modal-background" data-testid="background-close" onClick={props.onModalClose} />
      <div className="modal-content animated zoomIn fast">
        <div className="box">
          {props.children}
        </div>
      </div>
      <button className="modal-close is-large" data-testid="button-close" onClick={props.onModalClose} />
    </div>
  )
}

Modal.defaultProps = {
  onModalClose: () => {}
}
export default Modal
