import React from 'react'
import Modal from './Modal'
import Button from './Button'

const IssueModalContent = ({ showConfirmModal, issueModalText, issue, confirmIssue, cancelIssue, backToHome }) => {
  if (showConfirmModal) {
    return (
      <Modal onModalClose={backToHome}>
        <div className="content">
          <h1>{issueModalText.title}</h1>
          <h3>{issue}</h3>
          <p>{issueModalText.statement}</p>
          <div className="buttons is-right">
            <Button
              cancel
              id="button-cancel-issue"
              data-testid="button-cancel"
              onClick={cancelIssue}
            >
              {issueModalText.cancel}
            </Button>
            <Button
              confirm
              id="button-confirm-issue"
              onClick={() => { confirmIssue(issue) }}
            >
              {issueModalText.confirm}
            </Button>
          </div>
        </div>
      </Modal>
    )
  }
  return null
}

Modal.defaultProps = {
  show: false,
  issueModalText: {
    title: '',
    statement: '',
    confirm: ''
  },
  issue: '',
  confirmIssue: () => {},
  cancelIssue: () => {},
  backToHome: () => {}
}

export default IssueModalContent
