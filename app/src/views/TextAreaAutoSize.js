import React from 'react'
class TextAreaAutoSize extends React.Component {
  handleKeyUp (event) {
    if (event.key === 'Enter') {
      this.props.itemsubmit()
    }
  }

  render () {
    return (
      <textarea
        id={this.props.id}
        onKeyDown={this.props.onKeyDown}
        onChange={this.props.onChange}
        value={this.props.value}
        className='textarea'
        onKeyUp={this.handleKeyUp.bind(this)}
        placeholder={this.props.placeholder}
      />
    )
  }
}

TextAreaAutoSize.defaultProps = {
  itemsubmit: () => {},
  onChange: () => {},
  onKeyDown: () => {},
  id: '',
  placeholder: '',
  value: ''
}

export default TextAreaAutoSize
