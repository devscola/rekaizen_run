import Translations from '../services/Translations'
import Issues from '../services/Issues'
import ImprovementActions from '../services/ImprovementActions'
import ImpressionsService from '../services/Impressions'

import HomeContainer from '../containers/Home'
import ModalIssue from '../containers/Issue'
import ImprovementAction from '../containers/ImprovementAction'
import Impressions from '../containers/Impressions'
import TitleContainer from '../containers/Title'

class Home {
  constructor () {
    this.initializeContainers()
    this.initializeServices()
    this.startServices()
    this.renderPage()
  }

  initializeServices () {
    this.translationService = new Translations()
    new Issues()
    new ImprovementActions()
    new ImpressionsService()
  }

  initializeContainers () {
    this.home = new HomeContainer()
    this.issue = new ModalIssue()
    this.improvement = new ImprovementAction()
    this.impressions = new Impressions()
    this.titleContainer = new TitleContainer()
  }

  startServices () {
    this.translationService.retrieveTranslations()
  }

  renderPage () {
    // ReactDOM.render(<Title/>, document.querySelector('#title'))
    this.home.render()
    this.issue.render()
    this.improvement.render()
    this.impressions.render()
    this.titleContainer.render()
  }
}

export default Home
