import { Bus } from '../bus'
import { APIClient } from '../api_client'
import Collection from '../collections/Translations'
export default class Translations {
  constructor () {
    this.client = APIClient
    this.subscriptions()
  }

  subscriptions () {
  }

  retrieveTranslations () {
    console.log('publicado')
    Bus.publish('got.translations', Collection.retrieveAll())
  }
}
