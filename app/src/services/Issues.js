import { Bus } from '../bus'

export default class Issues {
  constructor () {
    this.subscriptions()
  }

  subscriptions () {
    Bus.subscribe('issue.confirm', this.saveAndPublish.bind(this))
  }

  saveAndPublish (payload) {
    this.save(payload)
    this.publish(payload.issue)
  }

  save (payload) {
    const issue = payload.issue
    this.storeData(issue)
  }

  publish (issue) {
    Bus.publish('issue.confirmed', { issue: issue })
  }

  storeData (key) {
    window.localStorage.setItem(key, JSON.stringify({}))
  }
}
