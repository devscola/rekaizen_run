class Translations {
  static retrieveAll () {
    return {
      home: {
        rekaizen: 'ReKaizen',
        startButton: 'Empieza!'
      },
      issue: {
        description: 'Un Tema es el objeto de análisis de una retrospectiva. Asegúrate que está propuesto de manera neutra y concisa para garantizar un análisis sin sesgos iniciales.',
        placeholder: 'Escriba el tema...',
        propose: 'Proponer',
        title: 'Éste es el tema',
        statement: 'Cómo squad confirmamos que queremos empezar el análisis de éste tema',
        confirm: 'Confirmar',
        cancel: 'Cancelar'
      },
      improvementAction: {
        title: 'Acciones de mejora',
        confirm: 'Añadir',
        placeholder: 'Introduce una acción de mejora SMART',
        buttonText: '+'
      },
      impression: {
        title: 'Impresiones',
        placeholder: 'Escriba una impresión...',
        buttonText: '+'
      }
    }
  }
}

export default Translations
