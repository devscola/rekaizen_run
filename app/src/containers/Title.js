import React from 'react'
import ReactDOM from 'react-dom'
import { Bus } from '../bus'

class Title {
  constructor (props) {
    this.state = {
      title: ''
    }
    this.subscribe()
  }

  subscribe () {
    Bus.subscribe('got.translations', this.setTitle.bind(this))
  }

  setTitle (params) {
    this.setState({ title: params.home.rekaizen })
  }

  setState (value) {
    this.state = Object.assign(this.state, value)
    this.render()
  }

  render () {
    ReactDOM.render(
      <a className="has-text-grey" href="#" id="rekaizen">{this.state.title}</a>,
      document.querySelector('#title')
    )
  }
}

export default Title
